<!-- FOOTER -->
<section class="container-fluid p-footer h-100">
  <div class="row align-items-center justify-content-center h-100">
      <div class="col-12 d-flex align-items-center justify-content-center mt-5">
          <ul class="nav d-flex align-items-center justify-content-center">
              <li class="nav-item mb-2">
                  <a href="#" class="nav-link px-5 fw-bold mc-black fs-14">Contattaci</a>
              </li>
              <li class="nav-item mb-2">
                  <a href="#" class="nav-link px-5 fw-bold mc-black fs-14">FAQs</a>
              </li>
              <li class="nav-item mb-2">
                  <a href="#" class="nav-link px-5 fw-bold mc-black fs-14">Chi siamo</a>
              </li>
              <li class="nav-item mb-2">
                  <a href="{{route('joinUs')}}" class="nav-link px-5 fw-bold mc-black fs-14">Join Us</a>
              </li>
          </ul>

      </div>
      <div class="col-12 d-flex align-items-center justify-content-center mb-5">
          <ul class="nav ">
              <li class="nav-item m-2">
                  <button class="btn mbg-pink  mc-black rounded-circle p-2 d-flex align-items-center"><i class="fa-regular fa-envelope"></i></button>                         
              </li>
              <li class="nav-item m-2">
                  <button class="btn mbg-pink  mc-black rounded-circle p-2 d-flex align-items-center"><i class="fa-regular fa-message"></i></button>
              </li>
              <li class="nav-item m-2">
                  <button class="btn mbg-pink  mc-black rounded-circle p-2 d-flex align-items-center"><i class="fa-regular fa-calendar-minus"></i></button>
              </li>
              <li class="nav-item m-2">
                  <button class="btn mbg-pink  mc-black rounded-circle p-2 d-flex align-items-center"><i class="fa-solid fa-shield-halved"></i></button>
              </li>
              <li class="nav-item m-2">
                  <button class="btn mbg-pink  mc-black rounded-circle p-2 d-flex align-items-center"><i class="fa-regular fa-clock"></i></button>
              </li>
          </ul>

      </div>
  </div>
</section>


