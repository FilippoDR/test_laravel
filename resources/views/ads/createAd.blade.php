<x-layout>
    <div class="container-fluid bg-container-fluid">
        <div class="container">
            <div class="container-ad-form">
                <h1 class="text-center mb-4">Crea il tuo annuncio</h1>
                <div class="row">
                    <div class="form-box col-11 col-md-8 col-lg-5 m-auto py-5 px-2 px-md-5">
                        <div class="w-100">
                            <livewire:ad-form></livewire:ad-form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-layout>